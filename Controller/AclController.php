<?php

/* vim: set expandtab tabstop=2 shiftwidth=2 softtabstop=2: */

class AclController extends AclAppController {
	var $name = 'Acl';

	var $uses = array('Acl.AclAco', 'Acl.AclAro');

	var $helpers = array('Html');

	public function admin_index() {
		$this->render('admin_index');
	}

	public function admin_aros() {
		$this->render('admin_aros');
	}

	public function admin_acos() {
		$this->render('admin_acos');
	}

	public function admin_permissions() {
		$this->render('admin_permissions');
	}
}